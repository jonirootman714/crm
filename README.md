# CRM

![crm preview](DOC/CRM-preview.jpg)

Это CRM система управления контактными данными клиентов разработана для вымышленной компании «SkillBus».

Реализован функционал:

- Возможность добавлять клиента в список, удалять, изменять
- Клиенту можно добавить контакты(телефон, почту и др)
- Возможность список по дате создания, изменения, по Фамилии, Имени и Отчеству
- Анимация открытия модального окна
- Валидация формы перед отправкой на сервер
- Индикация загрузки
- Поиск с автодополнением

## Запуск проекта

Перед запуском убедитесь, что вы установили Node.js версии 12 или выше.

- Для начала запустим сервер

```bash
cd server/
npm i
node index
```

- Затем запустим клиента в папке frontend используя расширение [LiveServer](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer)

## For developing

[Документация к серверу](server/README.md)

### libraries

- [Coices](https://github.com/Choices-js/Choices) - для выпадающих списков
- [Tippy.js](https://atomiks.github.io/tippyjs/v6/getting-started/) - tool-tip

Figma design: https://www.figma.com/file/rcta5K2ySOhnskjG1D82jL/CRM?node-id=121%3A485
