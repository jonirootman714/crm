# CRM

## API

- [Coices](https://github.com/Choices-js/Choices) - для выпадающих списков
- [Tippy.js](https://atomiks.github.io/tippyjs/v6/getting-started/) - tool-tip

## Реализован функционал

- Возможность добавлять клиента в список, удалять, изменять
- Клиенту можно добавить контакты(телефон, почту и др)
- Анимация открытия модального окна
- Валидация формы перед отправкой на сервер
- Индикация загрузки
- Поиск с автодополнением

## For developing

Figma design: https://www.figma.com/file/rcta5K2ySOhnskjG1D82jL/CRM?node-id=121%3A485
