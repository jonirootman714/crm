let currentChoices = [];
const listClients = document.getElementById("list-clients");

(() => {
  const btnAddPerson = document.getElementById("footer-create-person-btn");
  let currentIdClient = "";

  $.search({ container: document.querySelector(".header") });

  const madalComponents = {
    formPerson: [
      `
        <div class="form_info_person" id="form-add">
          <div class="section_form_input form_input-offest important_input-lastName">
            <input type="text" class="text form_input" id="input-lastName" placeholder="Фамилия*">
          </div>
          <div class="section_form_input form_input-offest important_input-name">
            <input type="text" class="text form_input" id="input-name" placeholder="Имя*">
          </div>
          <div class="section_form_input form_input-offest">
            <input type="text" class="text form_input" id="input-patronymic" placeholder="Отчество">
          </div>
        </div>
      `,
      `
        <div class="form_info_person" id="form-change">
          <div class="section_form_input">
            <label for="input-lastName" class="form_label">Фамилия*</label>
            <input type="text" class="text form_input" id="input-lastName">
          </div>
          <div class="section_form_input">
            <label for="input-name" class="form_label">Имя*</label>
            <input type="text" class="text form_input" id="input-name">
          </div>
          <div class="section_form_input">
            <label for="input-patronymic" class="form_label">Отчество</label>
            <input type="text" class="text form_input" id="input-patronymic">
          </div>
        </div>
      `,
    ],
    crateSectionContact(idList, idBtn) {
      return `
      <div class="form_contact_person">
        <div id="${idList}" class="form_contact_list"></div>
        <div class="form_contact_btn">
          <button id="${idBtn}" class="btn text btn--flex btn_contact_add active">
            <span class="btn_contact_add_circle">
              <svg class="btn_contact_add_svg" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M3.99996 0.666992C3.63329 0.666992 3.33329 0.966992 3.33329 1.33366V3.33366H1.33329C0.966626 3.33366 0.666626 3.63366 0.666626 4.00033C0.666626 4.36699 0.966626 4.66699 1.33329 4.66699H3.33329V6.66699C3.33329 7.03366 3.63329 7.33366 3.99996 7.33366C4.36663 7.33366 4.66663 7.03366 4.66663 6.66699V4.66699H6.66663C7.03329 4.66699 7.33329 4.36699 7.33329 4.00033C7.33329 3.63366 7.03329 3.33366 6.66663 3.33366H4.66663V1.33366C4.66663 0.966992 4.36663 0.666992 3.99996 0.666992Z" fill="#9873FF"/>
              </svg>
            </span>
            Добавить контакт
          </button>
        </div>
      </div>
    `;
    },
    sectionContactsItem: `
      <select class="select contact_select">
        <option>Телефон</option>
        <option>Email</option>
        <option>Facebook</option>
        <option>Vk</option>
        <option>Другое</option>
      </select>
      <input class="form_input contact_item_input" type="text" placeholder="Введите данные контакта">
      <button class="btn_contact_remove tooltip">
        <svg class="btn_contact_remove_svg" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path d="M6 0C2.682 0 0 2.682 0 6C0 9.318 2.682 12 6 12C9.318 12 12 9.318 12 6C12 2.682 9.318 0 6 0ZM6 10.8C3.354 10.8 1.2 8.646 1.2 6C1.2 3.354 3.354 1.2 6 1.2C8.646 1.2 10.8 3.354 10.8 6C10.8 8.646 8.646 10.8 6 10.8ZM8.154 3L6 5.154L3.846 3L3 3.846L5.154 6L3 8.154L3.846 9L6 6.846L8.154 9L9 8.154L6.846 6L9 3.846L8.154 3Z" fill="#F06A4D"/>
        </svg>
      </button>
    `,
    addInputItem(contactListId, contactBtn, log, type = "Телефон", value = "") {
      const typeChoices = type;
      const contactList = document.getElementById(contactListId);
      const el = document.createElement("div");
      addClass(el, ["form_contact_item"]);
      el.innerHTML = this.sectionContactsItem;
      el.children[2].addEventListener("click", removeInputItem);
      el.children[1].addEventListener("input", checkContactEmptyInput);
      contactList.append(el);
      this.checkCountInputs(contactList, contactBtn);
      el.children[1].value = value;

      const choices = new Choices(el.children[0], {
        searchEnabled: false,
        itemSelectText: "",
        position: "bottom",
        shouldSort: false,
        placeholder: false,
        allowHTML: true,
      });
      choices.setChoiceByValue(typeChoices);
      currentChoices.push(choices);

      const tooltip = tippy(el.children[2]);
      tooltip.setProps({
        content: `<strong class="tooltip_text_remove">Удалить контакт</strong>`,
        allowHTML: true,
        offset: [0, -3],
      });

      function removeInputItem() {
        choices.destroy();
        tooltip.destroy();
        el.children[2].removeEventListener("click", removeInputItem);
        el.children[1].removeEventListener("click", checkContactEmptyInput);
        el.remove(el);
        madalComponents.checkCountInputs(contactList, contactBtn);
      }

      function checkContactEmptyInput() {
        if (el.children[1].value === "") {
          addClass(el, ["input_contact_danger"]);
          document.getElementById(log).textContent = "Заполните пустое поле";
        } else {
          removeClass(el, "input_contact_danger");
          document.getElementById(log).textContent = "";
        }
      }
    },
    checkCountInputs(listElements, btn) {
      const button = btn;
      if (listElements.childNodes.length >= 10) {
        removeClass(button, "active");
      } else {
        if (!button.classList.contains("active")) {
          addClass(button, ["active"]);
        }
      }
    },
    createText(text, classStyle, id) {
      return `
      <div class="modal_text_warapper">
        <p id="${id}" class="${classStyle}">${text}</p>
      </div>
    `;
    },
  };

  function loadingFormStart(item) {
    item.disabled = true;
    item.children[0].style.display = "block";
  }

  function loadingFormStop(item) {
    item.disabled = false;
    item.children[0].style.display = "none";
  }

  const optionsModalChangePerson = {
    title: { value: "Изменить данные", align: "start" },
    secondTitle: `ID: `,
    closable: true,
    width: "450px",
    contentBody: `
      ${madalComponents.formPerson[1]}
      ${madalComponents.crateSectionContact(
        "modal-change-list",
        "modal-change-btnAdd"
      )}
      ${madalComponents.createText("", "log_danger", "change-log")}
    `,
    footerButtons: [
      {
        text: `Сохранить`,
        class: "modal_footer_btn--save",
        async handler() {
          if (
            checkEmptyValue("form-change", "modal-change-list", "change-log")
          ) {
            const infoCurrentClient = getClient(
              "form-change",
              "modal-change-list"
            );
            loadingFormStart(this);
            const result = await changeClient(
              infoCurrentClient,
              currentIdClient
            );
            if (result.ok) {
              loadingFormStop(this);
              modalChange.close();
              render();
            } else {
              loadingFormStop(this);
              document.getElementById("change-log").textContent =
                result.statusText + " " + result.status;
            }
          }
        },
      },
      {
        text: `Удалить клиента`,
        class: "modal_footer_btn_second",
        handler() {
          removeClient(currentIdClient);
          render();
          modalChange.close();
        },
      },
    ],
  };
  const modalChange = $.modal(optionsModalChangePerson);

  const optionsModalAddPerson = {
    title: { value: "Новый клиент", align: "start" },
    closable: true,
    width: "450px",
    contentBody: `
      ${madalComponents.formPerson[0]}
      ${madalComponents.crateSectionContact(
        "modal-add-list",
        "modal-add-btnAdd"
      )}
      ${madalComponents.createText("", "log_danger", "add-log")}
    `,
    footerButtons: [
      {
        text: `Сохранить`,
        class: "modal_footer_btn--save",
        async handler() {
          if (checkEmptyValue("form-add", "modal-add-list", "add-log")) {
            loadingFormStart(this);
            const response = await createClient(
              getClient("form-add", "modal-add-list")
            );
            if (response.ok) {
              loadingFormStop(this);
              render();
              modalAdd.close();
            } else {
              loadingFormStop(this);
              document.getElementById("add-log").textContent =
                response.statusText + " " + response.status;
            }
          }
        },
      },
      {
        text: `Отмена`,
        class: "modal_footer_btn_second",
        handler() {
          modalAdd.close();
        },
      },
    ],
  };
  const modalAdd = $.modal(optionsModalAddPerson);

  const optionsModalConfirm = {
    title: { value: "Удалить клиента", align: "center" },
    closable: true,
    contentBody: `
      ${madalComponents.createText(
        "Вы действительно хотите удалить данного клиента?",
        "modal_text"
      )}
      ${madalComponents.createText("", "log_danger", "confirm-log")}
    `,
    // offsetBottom: '0px',
    footerButtons: [
      {
        text: `Удалить`,
        class: "modal_footer_btn--save",
        async handler() {
          loadingFormStart(this);
          const response = await removeClient(currentIdClient);
          if (response.ok) {
            modalConfirm.close();
            loadingFormStop(this);
            render();
          } else {
            loadingFormStop(this);
            document.getElementById("confirm-log").textContent =
              response.statusText + " " + response.status;
          }
        },
      },
      {
        text: `Отмена`,
        class: "modal_footer_btn_second",
        handler() {
          modalConfirm.close();
        },
      },
    ],
  };
  const modalConfirm = $.modal(optionsModalConfirm);

  const btnContactInModalCreate = document.getElementById("modal-add-btnAdd");
  const btnContactInModalChange = document.getElementById(
    "modal-change-btnAdd"
  );

  btnContactInModalCreate.addEventListener("click", function () {
    madalComponents.addInputItem("modal-add-list", this, "add-log");
  });
  btnContactInModalChange.addEventListener("click", function () {
    madalComponents.addInputItem("modal-change-list", this, "change-log");
  });

  const formAddInputs = document.getElementById("form-add");
  const formChangeInputs = document.getElementById("form-change");
  addListenerInInputs(formAddInputs);
  addListenerInInputs(formChangeInputs);

  function addListenerInInputs(form) {
    let childNum;
    if (form.attributes.id.value === "form-add") childNum = 0;
    if (form.attributes.id.value === "form-change") childNum = 1;

    form.children[0].children[childNum].addEventListener(
      "input",
      checkEmptyInputListener
    );
    form.children[1].children[childNum].addEventListener(
      "input",
      checkEmptyInputListener
    );

    function checkEmptyInputListener() {
      if (this.value === "") {
        addClass(this, ["input_form_danger"]);
        this.parentElement.parentElement.parentElement.children[2].firstElementChild.textContent =
          "Заполните пустое поле";
      } else {
        removeClass(this, "input_form_danger");
        this.parentElement.parentElement.parentElement.children[2].firstElementChild.textContent =
          "";
      }
    }
  }

  listClients.addEventListener("click", (event) => {
    function loadingActionBtnStart(target, dilay = 100) {
      setTimeout(() => {
        target.disabled = true;
        target.children[0].style.display = "none";
        target.children[1].style.display = "block";
      }, dilay);
    }

    function loadingActionBtnStop(target, dilay = 300) {
      setTimeout(() => {
        target.disabled = false;
        target.children[0].style.display = "block";
        target.children[1].style.display = "none";
        modalChange.open();
      }, dilay);
    }

    if (event.target.classList.contains("btn_remove")) {
      modalConfirm.open();
      currentIdClient = event.target.dataset.id;
    }

    if (event.target.classList.contains("btn_change")) {
      loadingActionBtnStart(event.target);
      currentIdClient = event.target.dataset.id;
      if (loadClientInForm(currentIdClient, btnContactInModalChange)) {
        document.querySelector(".modal_second_title").textContent =
          "ID: " + currentIdClient;
        loadingActionBtnStop(event.target);
      }

      async function loadClientInForm(id, btn) {
        let statusFail;
        const currentClient = await loadClient(id);

        const inputs = document.getElementById("form-change").children;

        if (currentClient.message) {
          statusFail = true;
        } else {
          statusFail = false;
          inputs[0].children[1].value = currentClient.surname;
          inputs[1].children[1].value = currentClient.name;
          inputs[2].children[1].value = currentClient.lastName;

          currentClient.contacts.forEach((el) => {
            madalComponents.addInputItem(
              "modal-change-list",
              btn,
              "change-log",
              el.type,
              el.value
            );
          });
        }

        async function loadClient(id) {
          const response = await fetch(
            "http://localhost:3000/api/clients/" + id
          );
          const date = await response.json();
          return date;
        }

        return statusFail;
      }
    }
  });

  async function render(sort = { type: "id", value: "decreasing" }) {
    clearTable(listClients);
    isLoadingTable(true);

    function isLoadingTable(isLoading) {
      if (isLoading) {
        addClass(listClients, ["loading"]);
      } else {
        removeClass(listClients, "loading");
      }
    }

    function showError(err) {
      const containerError = document.createElement("div");
      const codeError = document.createElement("p");
      const messageError = document.createElement("p");
      addClass(containerError, ["error"]);
      addClass(codeError, ["error_code"]);
      addClass(messageError, ["error_message"]);
      codeError.textContent = err.status;
      messageError.textContent = err.data.message;
      isLoadingTable(false);
      containerError.append(messageError);
      containerError.append(codeError);
      listClients.append(containerError);
    }

    function clearError() {
      if (document.querySelector(".error")) {
        document.querySelector(".error").remove();
      }
    }

    const result = await loadData();
    const data = result.data;
    if (result.ok) {
      clearError();
      const arrClients = $.sort(data, sort);
      isLoadingTable(false);
      arrClients.forEach((el) => listClients.append(el));
      tooltipInit(".contacts-btns_item");
      function tooltipInit(tooltipClass) {
        const tooltips = document.querySelectorAll(tooltipClass);
        tooltips.forEach((el) => {
          tippy(el, {
            content: el.children[1],
            allowHTML: true,
            interactive: true,
          });
        });
      }
    } else {
      showError(result);
    }
  }
  render();

  btnAddPerson.addEventListener("click", () => {
    modalAdd.open();
  });

  function sortTable() {
    let idBtnIsActive = true;
    let nameBtnIsActive = false;
    let dateCreateBtnIsActive = false;
    let dateChangeBtnIsActive = false;

    const sortIdBtn = document.getElementById("btn-id");
    const sortFioBtn = document.getElementById("btn-fio");
    const sortDateCreateBtn = document.getElementById("btn-date-create");
    const sortDateChangeBtn = document.getElementById("btn-date-update");
    const sortBtns = document.querySelectorAll(".btn--sort");

    sortIdBtn.addEventListener("click", function () {
      if (idBtnIsActive) {
        idBtnIsActive = !idBtnIsActive;
        sortBtns.forEach((el) => removeClass(el, "active"));
        render({ type: "id", value: "ascending" });
      } else {
        idBtnIsActive = !idBtnIsActive;
        sortBtns.forEach((el) => removeClass(el, "active"));
        addClass(this, ["active"]);
        render({ type: "id", value: "decreasing" });
      }
    });
    sortFioBtn.addEventListener("click", function () {
      if (nameBtnIsActive) {
        nameBtnIsActive = !nameBtnIsActive;
        sortBtns.forEach((el) => removeClass(el, "active"));
        render({ type: "name", value: "ascending" });
      } else {
        nameBtnIsActive = !nameBtnIsActive;
        sortBtns.forEach((el) => removeClass(el, "active"));
        addClass(this, ["active"]);
        render({ type: "name", value: "decreasing" });
      }
    });
    sortDateCreateBtn.addEventListener("click", function () {
      if (dateCreateBtnIsActive) {
        dateCreateBtnIsActive = !dateCreateBtnIsActive;
        sortBtns.forEach((el) => removeClass(el, "active"));
        render({ type: "date-create", value: "ascending" });
      } else {
        dateCreateBtnIsActive = !dateCreateBtnIsActive;
        sortBtns.forEach((el) => removeClass(el, "active"));
        addClass(this, ["active"]);
        render({ type: "date-create", value: "decreasing" });
      }
    });
    sortDateChangeBtn.addEventListener("click", function () {
      if (dateChangeBtnIsActive) {
        dateChangeBtnIsActive = !dateChangeBtnIsActive;
        sortBtns.forEach((el) => removeClass(el, "active"));
        render({ type: "date-change", value: "ascending" });
      } else {
        dateChangeBtnIsActive = !dateChangeBtnIsActive;
        sortBtns.forEach((el) => removeClass(el, "active"));
        addClass(this, ["active"]);
        render({ type: "date-change", value: "decreasing" });
      }
    });
  }
  sortTable();

  function checkEmptyValue(idForm, idContactList, idLog) {
    function getInputsInForm(form) {
      let childNum;
      if (form.attributes.id.value === "form-add") childNum = 0;
      if (form.attributes.id.value === "form-change") childNum = 1;
      return [
        form.children[1].children[childNum],
        form.children[0].children[childNum],
        form.children[2].children[childNum],
      ];
    }

    const arrInputs = getInputsInForm(document.getElementById(idForm));
    const contactList = document.getElementById(idContactList);
    const log = document.getElementById(idLog);
    const contactListArr = Array.from(contactList.children);
    let correctInputs = 0;

    contactListArr.forEach((el) => {
      if (el.children[1].value === "") {
        addClass(el, ["input_contact_danger"]);
        log.textContent = "Заполните пустое поле";
        correctInputs++;
      }
    });

    arrInputs.forEach((el) => {
      if (el.id != "input-patronymic") {
        if (el.value === "") {
          addClass(el, ["input_form_danger"]);
          log.textContent = "Заполните пустое поле";
          correctInputs++;
        }
      }
    });

    return correctInputs === 0 ? true : false;
  }

  function getClient(idFormSection, idContactsSection) {
    const form = document.getElementById(idFormSection);
    let contactList = document.getElementById(idContactsSection);
    contactList = Array.from(contactList.children);

    return Object.assign(
      {
        contacts: getValuesFromContactList(contactList),
      },
      getInputValues(form)
    );
    function getInputValues(form) {
      let childNum;
      if (form.attributes.id.value === "form-add") childNum = 0;
      if (form.attributes.id.value === "form-change") childNum = 1;
      return {
        name: form.children[1].children[childNum].value,
        surname: form.children[0].children[childNum].value,
        lastName: form.children[2].children[childNum].value,
      };
    }
    function getValuesFromContactList(contactList) {
      let contacts = [];
      contactList.forEach((el) => {
        let contactItem = {};
        const select = el.children[0].children[0].children[0].value;
        const input = el.children[1].value;
        contactItem.type = select;
        contactItem.value = input;
        contacts.push(contactItem);
      });
      return contacts;
    }
  }

  async function createClient(client) {
    return await fetch("http://localhost:3000/api/clients/", {
      method: "POST",
      body: JSON.stringify({
        name: client.name,
        surname: client.surname,
        lastName: client.lastName,
        contacts: client.contacts,
      }),
    });
  }

  async function changeClient(client, idClient) {
    return await fetch("http://localhost:3000/api/clients/" + idClient, {
      method: "PATCH",
      body: JSON.stringify({
        name: client.name,
        surname: client.surname,
        lastName: client.lastName,
        contacts: client.contacts,
      }),
    });
  }

  async function removeClient(idClient) {
    return await fetch("http://localhost:3000/api/clients/" + idClient, {
      method: "DELETE",
    });
  }

  function clearTable(container) {
    container.innerHTML = "";
  }
})();

function addClass(item, classNames) {
  classNames.forEach((el) => {
    item.classList.add(el);
  });
}

function removeClass(item, className) {
  item.classList.remove(className);
}

async function loadData() {
  try {
    const response = await fetch("http://localhost:3000/api/clients");
    return {
      ok: response.ok,
      data: await response.json(),
      status: response.status,
    };
  } catch (e) {
    return {
      ok: false,
      data: { message: e },
      status: "",
    };
  }
}

function toClient(client) {
  function _createWrapTr() {
    const tr = document.createElement("tr");
    addClass(tr, ["table_tbody_box", "clients_table_tbody_box"]);
    tr.dataset.clientId = client.id;
    return tr;
  }

  function _createCell() {
    const td = document.createElement("td");
    addClass(td, ["table_tbody_item", "clients_table_tbody_item", "text"]);
    return td;
  }

  function _createSection(clientTitles, classNames = []) {
    const section = _createCell();
    addClass(section, classNames);
    let text = "";
    clientTitles.forEach((el) => (text += el + " "));
    section.textContent = text.trim();
    return section;
  }

  function _createSectionDate(clientDate) {
    const section = _createCell();
    const _date = Intl.DateTimeFormat("ru", {
      year: "numeric",
      month: "numeric",
      day: "numeric",
    }).format(Date.parse(clientDate));
    const _time = Intl.DateTimeFormat("ru", {
      hour: "numeric",
      minute: "numeric",
    }).format(Date.parse(clientDate));
    const span = document.createElement("span");
    addClass(span, ["clients_table_tbody_item_time", "text--gray"]);
    span.textContent = _time;
    section.textContent = _date + " ";
    section.append(span);
    return section;
  }

  function _createSectionContacts(clientContacts) {
    const contactSvgHtml = {
      phone: `
        <svg class="contacts-btns_item_svg" width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
          <g opacity="0.7">
            <circle cx="8" cy="8" r="8" fill="#9873FF"/>
            <path d="M11.56 9.50222C11.0133 9.50222 10.4844 9.41333 9.99111 9.25333C9.83556 9.2 9.66222 9.24 9.54222 9.36L8.84444 10.2356C7.58667 9.63556 6.40889 8.50222 5.78222 7.2L6.64889 6.46222C6.76889 6.33778 6.80444 6.16444 6.75556 6.00889C6.59111 5.51556 6.50667 4.98667 6.50667 4.44C6.50667 4.2 6.30667 4 6.06667 4H4.52889C4.28889 4 4 4.10667 4 4.44C4 8.56889 7.43556 12 11.56 12C11.8756 12 12 11.72 12 11.4756V9.94222C12 9.70222 11.8 9.50222 11.56 9.50222Z" fill="white"/>
          </g>
        </svg>
      `,
      another: `
        <svg class="contacts-btns_item_svg" width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path opacity="0.7" fill-rule="evenodd" clip-rule="evenodd" d="M8 16C12.4183 16 16 12.4183 16 8C16 3.58172 12.4183 0 8 0C3.58172 0 0 3.58172 0 8C0 12.4183 3.58172 16 8 16ZM3 8C3 5.24 5.24 3 8 3C10.76 3 13 5.24 13 8C13 10.76 10.76 13 8 13C5.24 13 3 10.76 3 8ZM9.5 6C9.5 5.17 8.83 4.5 8 4.5C7.17 4.5 6.5 5.17 6.5 6C6.5 6.83 7.17 7.5 8 7.5C8.83 7.5 9.5 6.83 9.5 6ZM5 9.99C5.645 10.96 6.75 11.6 8 11.6C9.25 11.6 10.355 10.96 11 9.99C10.985 8.995 8.995 8.45 8 8.45C7 8.45 5.015 8.995 5 9.99Z" fill="#9873FF"/>
        </svg>
      `,
      facebook: `
        <svg class="contacts-btns_item_svg" width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
          <g opacity="0.7">
            <path d="M7.99999 0C3.6 0 0 3.60643 0 8.04819C0 12.0643 2.928 15.3976 6.75199 16V10.3775H4.71999V8.04819H6.75199V6.27309C6.75199 4.25703 7.94399 3.14859 9.77599 3.14859C10.648 3.14859 11.56 3.30121 11.56 3.30121V5.28514H10.552C9.55999 5.28514 9.24799 5.90362 9.24799 6.53815V8.04819H11.472L11.112 10.3775H9.24799V16C11.1331 15.7011 12.8497 14.7354 14.0879 13.2772C15.3261 11.819 16.0043 9.96437 16 8.04819C16 3.60643 12.4 0 7.99999 0Z" fill="#9873FF"/>
          </g>
        </svg>
      `,
      email: `
        <svg class="contacts-btns_item_svg" width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path opacity="0.7" fill-rule="evenodd" clip-rule="evenodd" d="M8 16C12.4183 16 16 12.4183 16 8C16 3.58172 12.4183 0 8 0C3.58172 0 0 3.58172 0 8C0 12.4183 3.58172 16 8 16ZM4 5.75C4 5.3375 4.36 5 4.8 5H11.2C11.64 5 12 5.3375 12 5.75V10.25C12 10.6625 11.64 11 11.2 11H4.8C4.36 11 4 10.6625 4 10.25V5.75ZM8.424 8.1275L11.04 6.59375C11.14 6.53375 11.2 6.4325 11.2 6.32375C11.2 6.0725 10.908 5.9225 10.68 6.05375L8 7.625L5.32 6.05375C5.092 5.9225 4.8 6.0725 4.8 6.32375C4.8 6.4325 4.86 6.53375 4.96 6.59375L7.576 8.1275C7.836 8.28125 8.164 8.28125 8.424 8.1275Z" fill="#9873FF"/>
        </svg>
      `,
      vk: `
      <svg class="contacts-btns_item_svg" width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
        <g opacity="0.7">
          <path d="M8 0C3.58187 0 0 3.58171 0 8C0 12.4183 3.58187 16 8 16C12.4181 16 16 12.4183 16 8C16 3.58171 12.4181 0 8 0ZM12.058 8.86523C12.4309 9.22942 12.8254 9.57217 13.1601 9.97402C13.3084 10.1518 13.4482 10.3356 13.5546 10.5423C13.7065 10.8371 13.5693 11.1604 13.3055 11.1779L11.6665 11.1776C11.2432 11.2126 10.9064 11.0419 10.6224 10.7525C10.3957 10.5219 10.1853 10.2755 9.96698 10.037C9.87777 9.93915 9.78382 9.847 9.67186 9.77449C9.44843 9.62914 9.2543 9.67366 9.1263 9.90707C8.99585 10.1446 8.96606 10.4078 8.95362 10.6721C8.93577 11.0586 8.81923 11.1596 8.43147 11.1777C7.60291 11.2165 6.81674 11.0908 6.08606 10.6731C5.44147 10.3047 4.94257 9.78463 4.50783 9.19587C3.66126 8.04812 3.01291 6.78842 2.43036 5.49254C2.29925 5.2007 2.39517 5.04454 2.71714 5.03849C3.25205 5.02817 3.78697 5.02948 4.32188 5.03799C4.53958 5.04143 4.68362 5.166 4.76726 5.37142C5.05633 6.08262 5.4107 6.75928 5.85477 7.38684C5.97311 7.55396 6.09391 7.72059 6.26594 7.83861C6.45582 7.9689 6.60051 7.92585 6.69005 7.71388C6.74734 7.57917 6.77205 7.43513 6.78449 7.29076C6.82705 6.79628 6.83212 6.30195 6.75847 5.80943C6.71263 5.50122 6.53929 5.30218 6.23206 5.24391C6.07558 5.21428 6.0985 5.15634 6.17461 5.06697C6.3067 4.91245 6.43045 4.81686 6.67777 4.81686L8.52951 4.81653C8.82136 4.87382 8.88683 5.00477 8.92645 5.29874L8.92808 7.35656C8.92464 7.47032 8.98521 7.80751 9.18948 7.88198C9.35317 7.936 9.4612 7.80473 9.55908 7.70112C10.0032 7.22987 10.3195 6.67368 10.6029 6.09801C10.7279 5.84413 10.8358 5.58142 10.9406 5.31822C11.0185 5.1236 11.1396 5.02785 11.3593 5.03112L13.1424 5.03325C13.195 5.03325 13.2483 5.03374 13.3004 5.04274C13.6009 5.09414 13.6832 5.22345 13.5903 5.5166C13.4439 5.97721 13.1596 6.36088 12.8817 6.74553C12.5838 7.15736 12.2661 7.55478 11.9711 7.96841C11.7001 8.34652 11.7215 8.53688 12.058 8.86523Z" fill="#9873FF"/>
        </g>
      </svg>
      `,
    };

    function _choiceSvgOnType(svgHtml, type) {
      switch (type) {
        case "Телефон":
          return svgHtml.phone;
        case "Email":
          return svgHtml.email;
        case "Facebook":
          return svgHtml.facebook;
        case "Vk":
          return svgHtml.vk;
        case "Другое":
          return svgHtml.another;
      }
    }

    function _createTooltipItem(contact) {
      const container = document.createElement("p");
      addClass(container, ["contacts-btns_item_p"]);
      const link = document.createElement("a");
      link.target = "_blank";

      switch (contact.type) {
        case "Телефон":
          link.href = `tel:${contact.value}`;
          link.textContent = contact.value.trim().toLowerCase();
          addClass(link, [
            "link",
            "contacts-btns_item_link",
            "contacts-btns_item_link-primary",
          ]);
          break;
        case "Email":
          link.href = `mailto:${contact.value}`;
          link.textContent = contact.value.trim().toLowerCase();
          addClass(link, [
            "link",
            "contacts-btns_item_link",
            "contacts-btns_item_link-primary",
          ]);
          break;
        case "Facebook":
          link.href = contact.value;
          link.textContent = contact.value
            .trim()
            .toLowerCase()
            .replace("https://www.facebook.com/", "@");
          addClass(link, ["link", "contacts-btns_item_link"]);
          container.textContent = contact.type + ": ";
          break;
        case "Vk":
          link.href = contact.value;
          link.textContent = contact.value
            .trim()
            .toLowerCase()
            .replace("https://vk.com/", "@");
          addClass(link, ["link", "contacts-btns_item_link"]);
          container.textContent = contact.type + ": ";
          break;
        case "Другое":
          link.href = contact.value;
          link.textContent = contact.value.trim().toLowerCase();
          addClass(link, ["link", "contacts-btns_item_link"]);
          container.textContent = contact.type + ": ";
          break;
      }

      container.append(link);
      return container;
    }

    function _createContacts(contacts) {
      const container = document.createElement("div");
      addClass(container, ["contacts-btns"]);

      contacts.forEach((contact) => {
        const wrapContact = document.createElement("button");
        addClass(wrapContact, ["contacts-btns_item", "btn"]);
        const item = document.createElement("div");
        item.innerHTML = _choiceSvgOnType(contactSvgHtml, contact.type);

        wrapContact.append(item.children[0]);
        wrapContact.append(_createTooltipItem(contact));

        container.append(wrapContact);
      });

      return container;
    }

    const section = _createCell();
    section.append(_createContacts(clientContacts));
    return section;
  }

  function _createSectionBtns(client) {
    const section = _createCell();
    const div = document.createElement("div");
    addClass(div, ["action"]);

    const buttonsHtml = `
      <button class="btn btn--flex bnt--edit action_btn btn_change" data-id="${client.id}">
        <svg class="btn_svg action_btn_svg" width="13" height="13" viewbox="0 0 13 13" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path d="M0 10.5002V13.0002H2.5L9.87333 5.62687L7.37333 3.12687L0 10.5002ZM11.8067 3.69354C12.0667 3.43354 12.0667 3.01354 11.8067 2.75354L10.2467 1.19354C9.98667 0.933535 9.56667 0.933535 9.30667 1.19354L8.08667 2.41354L10.5867 4.91354L11.8067 3.69354Z" fill="#9873FF"/>
        </svg>
        <svg class="btn_svg action_btn_svg action_btn_svg--edit action_btn_svg--loading" width="13" height="13" viewbox="0 0 13 13" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path d="M1.00008 6.04008C1.00008 8.82356 3.2566 11.0801 6.04008 11.0801C8.82356 11.0801 11.0801 8.82356 11.0801 6.04008C11.0801 3.2566 8.82356 1.00008 6.04008 1.00008C5.38922 1.00008 4.7672 1.12342 4.196 1.34812" stroke="#9873FF" stroke-width="2" stroke-miterlimit="10" stroke-linecap="round"/>
        </svg>
        Изменить
      </button>
      <button class="btn btn--flex bnt--danger action_btn btn_remove" data-id="${client.id}">
        <svg class="btn_svg action_btn_svg" width="12" height="12" viewbox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path d="M6 0C2.682 0 0 2.682 0 6C0 9.318 2.682 12 6 12C9.318 12 12 9.318 12 6C12 2.682 9.318 0 6 0ZM6 10.8C3.354 10.8 1.2 8.646 1.2 6C1.2 3.354 3.354 1.2 6 1.2C8.646 1.2 10.8 3.354 10.8 6C10.8 8.646 8.646 10.8 6 10.8ZM8.154 3L6 5.154L3.846 3L3 3.846L5.154 6L3 8.154L3.846 9L6 6.846L8.154 9L9 8.154L6.846 6L9 3.846L8.154 3Z" fill="#F06A4D"/>
        </svg>
        <svg class="btn_svg action_btn_svg action_btn_svg--danger action_btn_svg--loading" width="13" height="13" viewbox="0 0 13 13" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path d="M1.00008 6.04008C1.00008 8.82356 3.2566 11.0801 6.04008 11.0801C8.82356 11.0801 11.0801 8.82356 11.0801 6.04008C11.0801 3.2566 8.82356 1.00008 6.04008 1.00008C5.38922 1.00008 4.7672 1.12342 4.196 1.34812" stroke="#9873FF" stroke-width="2" stroke-miterlimit="10" stroke-linecap="round"/>
        </svg>
        Удалить
      </button>
    `;
    div.innerHTML = buttonsHtml;
    section.append(div);
    return section;
  }

  const clientHtml = _createWrapTr();
  clientHtml.append(_createSection([client.id], ["text--gray"]));
  clientHtml.append(
    _createSection([client.surname, client.name, client.lastName])
  );
  clientHtml.append(_createSectionDate(client.createdAt));
  clientHtml.append(_createSectionDate(client.updatedAt));
  clientHtml.append(_createSectionContacts(client.contacts));
  clientHtml.append(_createSectionBtns(client));

  return clientHtml;
}
