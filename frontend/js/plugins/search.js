$.search = function(options) {
  const select = document.createElement('select')
  select.setAttribute('multiple', '')
  options.container.append(select)
  const choices = new Choices(select, {
    searchEnabled: false,
    searchChoices: false,
    itemSelectText: '',
    placeholder: true,
    placeholderValue: 'Введите запрос',
    choices: [],
    renderChoiceLimit: '10',
    maxItemCount: '1',
    removeItemButton: true,
    noChoicesText: 'Клиент не найден',
  })
  addClass(select.parentNode, ['search_input'])
  addClass(select.parentNode.parentNode, ['search'])
  select.nextSibling.nextSibling.addEventListener('input', selectHeandler)
  select.nextSibling.nextSibling.addEventListener('click', selectHeandler)

  let inputTimeout
  function selectHeandler(target) {
    clearTimeout(inputTimeout)
    inputTimeout = setTimeout(() => {
      const inputValue = target.target.value
      .trim()
      .toLowerCase()
      .replace(/\s/g, '')

      loadDataInput()
      .then(
        function result(result) {
          if (result.ok) {
            const choicesValuesArr = result.data.map(el => {
              return {
                value: (el.name + el.surname).trim().toLowerCase(),
                label: el.name + ' ' + el.surname,
                customProperties: {
                  desctiption: el.id
                }
              }
            })
            const finalArr = []
            choicesValuesArr.forEach(el => {
              if(el.value.includes(inputValue)) {
                finalArr.push(el)
              }
            })
            choices.setChoices(
              finalArr,
              'value',
              'label',
              true,
            )
          } else {}
        }
      )
    }, 300)

  }

  select.addEventListener('addItem', function(event) {
    const choiceClientId = event.detail.customProperties.desctiption
    const currentClient = document.querySelector(`[data-client-id="${choiceClientId}"]`)
    currentClient.scrollIntoView()
    addClass(currentClient, ['celect'])
    setTimeout(()=> {
      removeClass(currentClient, 'celect')
    }, 1000)
  })

}

async function loadDataInput() {
  return await loadData()
}