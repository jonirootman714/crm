$.sort = function(data, sort) {
  let sortedArr = []
  const finallArr = []

  switch(sort.type) {
    case 'id':
      if(sort.value === 'ascending') {
        sortedArr = data.slice().sort((a, b) => {
          if (a.id < b.id) {
            return 1;
          }
          if (a.id > b.id) {
            return -1;
          }
        })
      }
      if(sort.value === 'decreasing') {
        sortedArr = data.slice().sort((a, b) => {
          if (a.id > b.id) {
            return 1;
          }
          if (a.id < b.id) {
            return -1;
          }
        })
      }
      break
    case 'name':
      if(sort.value === 'ascending') {
        sortedArr = data.slice().sort((a, b) => {
          if (a.surname < b.surname) {
            return 1;
          }
          if (a.surname > b.surname) {
            return -1;
          }
        })
      }
      if(sort.value === 'decreasing') {
        sortedArr = data.slice().sort((a, b) => {
          if (a.surname > b.surname) {
            return 1;
          }
          if (a.surname < b.surname) {
            return -1;
          }
        })
      }
      break
    case 'date-create':
      if(sort.value === 'ascending') {
        sortedArr = data.slice().sort((a, b) => b.createdAt < a.createdAt)
      }
      if(sort.value === 'decreasing') {
        sortedArr = data.slice().sort((a, b) => b.createdAt > a.createdAt)
      }
      break
    case 'date-change':
      if(sort.value === 'ascending') {
      sortedArr = data.slice().sort((a, b) => b.updatedAt < a.updatedAt)
      }
      if(sort.value === 'decreasing') {
        sortedArr = data.slice().sort((a, b) => b.createdAt > a.createdAt)
      }
    default:

      break
  }

  sortedArr.forEach(el => {
    finallArr.push(toClient(el))
  })

  return finallArr
}