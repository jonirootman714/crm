Element.prototype.appendAfter = function(el) {
  el.parentNode.insertBefore(this, el.nextSibling)
}

function noop() {}

function _createFooter(buttons = []) {
  if (buttons.length === 0) ''

  const wrap = document.createElement('div')
  addClass(wrap, ['section_form', 'modal_footer'])

  buttons.forEach(btn => {
    const _btn = document.createElement('button')
    _btn.innerHTML = `
      <svg class="modal_footer_btn_lading-svg" width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M3.00008 8.03996C3.00008 10.8234 5.2566 13.08 8.04008 13.08C10.8236 13.08 13.0801 10.8234 13.0801 8.03996C13.0801 5.25648 10.8236 2.99996 8.04008 2.99996C7.38922 2.99996 6.7672 3.1233 6.196 3.348" stroke="#B89EFF" stroke-width="2" stroke-miterlimit="10" stroke-linecap="round"/>
      </svg>
      ${btn.text}
    `
    addClass(_btn, ['btn', 'btn--primary', `${btn.class}`])
    _btn.onclick = btn.handler || noop

    wrap.appendChild(_btn)
  })
  return wrap
}

function _createModal(options) {
  function onClose() {
    modal.destroy()
  }
  const DEFOALT_WIDTH = '450px'
  const modal = document.createElement('div')
  addClass(modal, ['modal'])

  modal.insertAdjacentHTML('afterbegin', `
    <div class="modal_overlay" data-close="true">
      <div class="modal_window" style="width: ${options.width || DEFOALT_WIDTH}">
        <div class="modal_form">
          <div class="section_form modal_header modal_header--${options.title.align}">
            <span class="title modal_title">${options.title.value || ''}</span><span class="text text--gray modal_second_title">${options.secondTitle || ''}</span>
            ${options.closable ? `
              <button data-close="true" class="btn modal_close">
                <svg data-close="true" class="modal_close_svg" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path data-close="true" fill-rule="evenodd" clip-rule="evenodd" d="M22.2333 7.73333L21.2666 6.76666L14.4999 13.5334L7.73324 6.7667L6.76658 7.73336L13.5332 14.5L6.7666 21.2667L7.73327 22.2333L14.4999 15.4667L21.2666 22.2334L22.2332 21.2667L15.4666 14.5L22.2333 7.73333Z" fill="#B0B0B0"/>
                </svg>
              </button>
            ` : ''}
          </div>
          <div class="section_form modal_body" data-content style="margin-bottom: ${options.offsetBottom || '0px'}">
            ${options.contentBody || ''}
          </div>
        </div>
      </div>
    </div>
  `)
  const footer = _createFooter(options.footerButtons)
  footer.appendAfter(modal.querySelector('[data-content]'))
  document.body.appendChild(modal)
  return modal
}

$.modal = function(options) {
  const ANIMATION_SPEED = 300
  const $modal = _createModal(options)
  let closing = false
  let destroyed = false

  const modal = {
    open() {
      if (destroyed) {
        return console.log('Modal is destroed')
      }
      !closing && addClass($modal, ['open'])
    },
    close() {
      closing = true
      addClass($modal, ['hide'])
      removeClass($modal, 'open')
      setTimeout(() => {
        removeClass($modal, 'hide')
        closing = false
        if (typeof options.onClose === 'function') {
          options.onClose()
        }
        clearForm()

        function clearForm() {
          const inputs = Array.from(document.querySelectorAll('.form_input'))
          const contactList = Array.from(document.querySelectorAll('.form_contact_list'))
          const logs = Array.from(document.querySelectorAll('.log_danger'))
          currentChoices.forEach(el => {
            el.destroy()
            currentChoices = []
          })

          contactList.forEach(el => el.innerHTML = '')
          inputs.forEach(el => el.value = '')
          logs.forEach(el => el.textContent = '')

          inputs.forEach(el => {
            if(el.classList.contains('input_form_danger')) {
              removeClass(el, 'input_form_danger')
            }
          })

        }
      }, ANIMATION_SPEED)

    }
  }

  const listener = event => {
    if (event.target.dataset.close) {
      modal.close()
    }
  }

  $modal.addEventListener('click', listener)

  return Object.assign(modal, {
    destroy() {
      $modal.parentNode.removeChild($modal)
      $modal.removeEventListener('click', listener)
      destroyed = true
    }
  })
}



